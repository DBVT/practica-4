package mx.unitec.moviles.practica4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var rfc = intent.getStringExtra(EXTRA_RFC)

        if (rfc != null)
            DataPreferences.setStoredRFC(this, rfc)
        else
            rfc = DataPreferences.getStoredRFC(this)

        viewRFC.text = rfc

        Toast.makeText(this, "RFC: $rfc", Toast.LENGTH_LONG).show()
    }
}